This module is an add-on for the Bio module, it automatically adds taxonomy terms to the Bio node based on the Bio nodes owners role.

Why?

With the Bio node assigned taxonomy terms with the same names as the rolse of its owner, it opens up a number of opportunities :

    * Listing profiles by role
    * Restricting access to profiles by both who is viewing the profile and who owns the profile (with the help of Taxonomy Access Control)
    * ... and probably other things, answers on a postcard please :)


How?

The module updates the Bio node every time the user or the bio node itself is saved. There is also a bulk update feature to process all Bio nodes at once.

Setup is very simple,

   1. Download and install the module
   2. Create a new vocabulary and insert some terms which match the names you have for roles e.g. admin
   3. Assign the new vocabulary to the Bio content type
   4. Go to the Bio role terms admin page (admin/user/bio_role_terms) and select the correct vocabulary, then save
   5. Optionally, perform a bulk update.


This is a very new module and not fully community tested, so backup first. Also any budding coders out there, we have 3 TODO items in the code which would love some attention.
Patches, new features, issues etc all very welcome

Sponsored by OpenlyConnected

Hope you find it helpful.

a_c_m

openlyconnected.com